module Main where

import Control.Lens
import Linear
import Data.Monoid
import Graphics.Gloss

data Triangle = Triangle
  { _a :: {-# UNPACK #-} !(V2 Rational)
  , _b :: {-# UNPACK #-} !(V2 Rational)
  , _c :: {-# UNPACK #-} !(V2 Rational)
  }
  deriving Show

data Model = Model
  { current :: Triangle
  , path :: [Point]
  }

stepTriangle :: Triangle -> Triangle
stepTriangle (Triangle a b c) = Triangle (b * 1.6) (c * 1.6) $ ((a + b + c) / 3) * 1.6

toPoint :: V2 Rational -> Point
toPoint (V2 x y) = (realToFrac x, realToFrac y)

applyZoom :: Double -> Point -> Point
applyZoom z (x,y) = (realToFrac z * x, realToFrac z * y);

step :: Model -> Model
step (Model triangle p) = Model triangle' (toPoint (_a triangle) : map (applyZoom 1.6) (take 100 p)) where
  triangle' :: Triangle
  triangle' = stepTriangle triangle

draw :: Model -> Picture
draw (Model _ p) = color azure $ line p

startTriangle :: Triangle
startTriangle = Triangle (-6) (V2 3 6) (V2 0 (-2))

main :: IO ()
main = simulate (InWindow "bwm" (800,800) (0,0)) (greyN 0.7) 15 (Model startTriangle [toPoint $ _c startTriangle]) draw $ const $ const step
